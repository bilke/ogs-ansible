# Setup

```bash
vim  .vault_pass # [copy 1p: ansible-vault]
brew install azure-cli
az login
poetry install
poetry run ang install --force -r roles/requirements.yml -p roles.galaxy
```

## Vault

```bash
ansible-vault edit group_vars/all/vault
```

# Usage

```bash
poetry shell
ansible-playbook playbooks/gitlab.yml
```

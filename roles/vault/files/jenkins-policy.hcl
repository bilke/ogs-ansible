path "jenkins/*" {
  capabilities = ["create","read"]
}

path "auth/token/lookup-self" {
  capabilities = ["read"]
}

SECRET_KEY = '{{ sregistry_secret_key }}'

SOCIAL_AUTH_GITHUB_KEY = '{{ sregistry_github_key }}'
SOCIAL_AUTH_GITHUB_SECRET = '{{ sregistry_github_secret }}'

SOCIAL_AUTH_GITLAB_SCOPE = ['api', 'read_user']
SOCIAL_AUTH_GITLAB_KEY = '{{ sregistry_gitlab_key }}'
SOCIAL_AUTH_GITLAB_SECRET = '{{ sregistry_gitlab_secret }}'
SOCIAL_AUTH_GITLAB_API_URL = '{{ sregistry_gitlab_url }}'

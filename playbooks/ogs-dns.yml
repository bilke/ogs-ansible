# Login with:
# az account clear
# az login
# az account set --subscription "Microsoft Azure Sponsorship"
---
- hosts: localhost
  connection: local
  vars:
    resource_group: ogs
    ip_gitlab: 13.95.94.101
  tasks:
  - name: Create OGS DNS zone
    azure_rm_dnszone:
      resource_group: "{{ resource_group }}"
      name: opengeosys.org
  - name: Create OGS records
    azure_rm_dnsrecordset:
      resource_group: ogs
      zone_name: opengeosys.org
      state: present
      relative_name: "{{ item.name }}"
      record_type: "{{ item.type }}"
      records: "{{ item.records }}"
    with_items:
      - { name: '@', type: 'TXT', records: [ { entry: "google-site-verification=_dugOauMqKahdK1xW0YjDq_E3KTM7Bn51ggaliTHanQ" } ] }
      # Mailgun
      - { name: 'mg', type: 'TXT', records: [ { entry: "v=spf1 include:eu.mailgun.org ~all" } ] }
      - { name: 'k1._domainkey.mg', type: 'TXT', records: [ { entry: "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC8sOJpJavPEMqpU9Yzr+NzjE5cbBg9gRtQZ0lL7hfEScqL2wmh0qLbdG1oE9GkIWbP5EffE4Tp13YgP1MNXrL9Z2Xfps1WcUE3FEk7ge39WHaMrMfMgC07BDCHj+OToWaFhjNkQ+zqR493IoxajLO/Qj/Co85suqvigZPN7fb/DQIDAQAB" } ] }
      - { name: 'mg', type: 'TXT', records: [ { entry: "v=spf1 include:eu.mailgun.org ~all" } ] }
      - { name: 'mg', type: 'MX', records: [ { entry: 'mxa.eu.mailgun.org', preference: 10 }, { entry: 'mxb.eu.mailgun.org', preference: 20 } ] }
      - { name: 'email.mg', type: 'CNAME', records: [ { entry: "eu.mailgun.org" } ] }
      #
      - { name: '*.review', type: 'A', records: [ { entry: "104.45.41.241" } ] }
      - { name: 'www', type: 'CNAME', records: [ { entry: "ogs.netlify.com." } ] }
      - { name: '@', type: 'A', records: [ { entry: "104.198.14.52" } ] } # Root (apex) domain
      - { name: 'benchmarks', type: 'CNAME', records: [ { entry: "ogs.netlify.com." } ] }
      - { name: 'consul', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'nomad', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'discourse', type: 'A', records: [ { entry: "104.40.180.188" } ] }
      - { name: 'docs', type: 'CNAME', records: [ { entry: "ogs.netlify.com." } ] }
      - { name: 'doxygen', type: 'CNAME', records: [ { entry: "doxygen.netlify.app." } ] }
      - { name: 'doxygen-latest', type: 'CNAME', records: [ { entry: "doxygen.netlify.app." } ] }
      - { name: 'gitlab-netdata', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'gitlab', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'logs', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'mattermost', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'prometheus', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'alerts', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'registry', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'tutorials', type: 'CNAME', records: [ { entry: "ogs.netlify.com." } ] }
      - { name: 'vault', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'meet', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'plausible', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'code', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 's3', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 's3-api', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      - { name: 'data', type: 'A', records: [ { entry: "13.80.177.40" } ] }
      - { name: 'jupyter', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }
      # Exchange Online
      - { name: '@', type: 'MX', records: [ { entry: 'opengeosys-org.mail.protection.outlook.com', preference: 0 } ] }
      # does not work yet: https://github.com/ansible/ansible/pull/38368
      # - { name: '@', type: 'TXT', records: [ { entry: 'v=spf1 include:spf.protection.outlook.com -all' } ] }
      - { name: 'autodiscover', type: 'CNAME', records: [ { entry: autodiscover.outlook.com} ] }
      # bilke netlify deploy preview
      - { name: 'bilke', type: 'CNAME', records: [ { entry: "bilke-ogs.netlify.com." } ] }
  - name: Create OGS XYZ DNS zone
    azure_rm_dnszone:
      resource_group: "{{ resource_group }}"
      name: ogs.xyz
  - name: Create OGS XYZ records
    azure_rm_dnsrecordset:
      resource_group: ogs
      zone_name: ogs.xyz
      state: present
      relative_name: "{{ item.name }}"
      record_type: "{{ item.type }}"
      records: "{{ item.records }}"
    with_items:
      - { name: '@', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] } # Root (apex) domain
      - { name: '*', type: 'A', records: [ { entry: "{{ ip_gitlab }}" } ] }

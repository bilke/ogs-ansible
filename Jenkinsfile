def playbooks = [
  "consul",
  "gitlab",
  "jenkins-slaves",
  "jenkins",
  "ogs-dns",
  "vault"
]

def jobs = [:]
playbooks.each {
  jobs["${it}"] = {
    node('docker') {
      def image = docker.image('ogs6/misc_ansible:latest')
      image.pull()
      image.inside() {
        checkout scm
        sshagent(credentials: ['ansible-ssh']) {
          withEnv(['ANSIBLE_HOST_KEY_CHECKING=False']) {
            withCredentials([
              file(credentialsId: 'ansible-vault-pass', variable: 'VAULT_PASS_FILE'),
              string(credentialsId: 'azure-client-id', variable: 'AZURE_CLIENT_ID'),
              string(credentialsId: 'azure-client-secret', variable: 'AZURE_SECRET'),
              string(credentialsId: 'azure-subscription-id', variable: 'AZURE_SUBSCRIPTION_ID'),
              string(credentialsId: 'azure-tenant-id', variable: 'AZURE_TENANT')]) {
              sh 'ansible-galaxy install -r roles/requirements.yml -p ./roles.galaxy'
              sh "ansible-playbook playbooks/${it}.yml --vault-password-file $VAULT_PASS_FILE"
            }
          }
        }
      }
    }
  }
}
parallel jobs
